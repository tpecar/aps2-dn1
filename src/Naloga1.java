import java.util.Scanner;

public class Naloga1
{
    /* od zunanjih knjiznic je dovoljen le Scanner */
    //static Scanner vhod = new Scanner(System.in);
    
    static int stPrimerjav = 0;
    static int stPrirejanj = 0;
    
    /* primerja bodisi < (primerjava=true) ali > (false) */
    static boolean urejen(int prvaVrednost, int drugaVrednost, boolean primerjava)
    {
        stPrimerjav++;
        return (primerjava ? prvaVrednost<drugaVrednost : prvaVrednost>drugaVrednost);
    }
    /* v osnovi no-op, vrne dobljeni element ter poveca stevec */
    static int priredi(int vrednost)
    {
        stPrirejanj++;
        return vrednost;
    }
    
    /* uredi podano tabelo s podanim algoritmom */
    static int[] uredi(int[] tabela, String algoritem, boolean sled, boolean red) throws Exception
    {
        switch(algoritem)
        {
            case "bs" : return bubbleSort(tabela, sled, red);
            case "ss" : return selectionSort(tabela, sled, red);
            case "is" : return insertionSort(tabela, sled, red);
            case "hs" : return heapSort(tabela, sled, red);
            case "qs" : return quickSort(tabela, sled, red);
            case "ms" : return mergeSort(tabela, sled, red);
            
            default : throw new Exception("Neveljavno razvrscanje!");
        }
    }
    
    public static void main(String[] args) throws Exception
    {
        /* za namene testiranja */
        //args = new String[]{"count","ms","up"};
        //Scanner vhod = new Scanner(new FileReader("test-C-1.out"));
        Scanner vhod = new Scanner(System.in);
        
        /*
         * Prvi trije argumenti so vedno prisotni ter dolocajo naslednje
         * 1. nacin delovanja (trace, count)
         * 2. algoritem sortiranja
         *      bs - bubble sort
         *      ss - selection sort
         *      is - insertion sort
         *      hs - heap sort
         *      qs - quick sort
         *      ms - merge sort
         * 3. smer sortiranja (up, down)
         * 4. velikost tabele (ki je popolnoma neuporaben, saj lahko ni podan,
         *    lahko pa je podan ter predvideva manj podatkov kot jih dobimo
         *    na vhodu) - ignoriramo
         */
        
        /* ponastavimo stevce */
        stPrimerjav = 0;
        stPrirejanj = 0;
        
        /* ce true, sortiranje po narascajocih vrednostih,
           ce false, sortiranje po padajocih vrednostih */
        if(args.length>=3)
        {
            boolean red = args[2].equals("up");

            /* preberemo vhodno tabelo */
            Buffer buffer = new Buffer();
            while(vhod.hasNextInt())
                buffer.dodajElement(vhod.nextInt());
                   
            switch(args[0])
            {
                /* vedno klicemo da iz bufferja ustvari novo tabelo, ker se
                   pri urejanju spremeni (je le referenca) */
                case "trace" : uredi(buffer.pretvori(), args[1], true, red); break;/* za namen testiranja */
                case "count" :
                {
                    stPrimerjav = stPrirejanj = 0;
                    /* uredimo neurejeno tabelo */
                    int[] urejeno = uredi(buffer.pretvori(), args[1], false, red);
                    int primNeurejeno = stPrimerjav;
                    int prirNeurejeno = stPrirejanj;
                    stPrimerjav = stPrirejanj = 0;
                    
                    /* uredimo urejeno - tabela se ne sme spremeniti! */
                    uredi(urejeno, args[1], false, red);
                    int primUrejeno = stPrimerjav;
                    int prirUrejeno = stPrirejanj;
                    stPrimerjav = stPrirejanj = 0;
                    
                    /* uredimo v obratni smeri */
                    uredi(urejeno, args[1], false, !red);
                    
                    /* izpisemo statistiko */
                    System.out.format("%d %d\n%d %d\n%d %d\n",
                            primNeurejeno, prirNeurejeno,
                            primUrejeno, prirUrejeno,
                            stPrimerjav, stPrirejanj);
                }
            }
            
        }
        else
            System.out.println("Manjkajo argumenti!");
    }
    
    /*  sled - ali naj izpise sled 
        red - ali se razvrsca navzgor (true) ali navzdol (false) */
    static int[] bubbleSort(int[] tabela, boolean sled, boolean red)
    {
        /*  zahtevan bubbleSort razdeli tabelo na urejen ter neurejen del,
            pri cemer menjavamo elemente neurejenega dela od zadnjega dela
            naprej - element, ki priplava do dna neurejenega dela se doda
            na vrh urejenega */
        for(int dolzUrejeni=0; dolzUrejeni<tabela.length; dolzUrejeni++)
        {
            if(sled)
            {
                /* ko gremo skozi celotno tabelo izpisemo novo stanje */
                for(int i=0; i<tabela.length; i++)
                    System.out.printf((i==dolzUrejeni ? "| %d ":"%d "), tabela[i]);
                System.out.println();
            }
            for(int i=tabela.length-1; i>dolzUrejeni; i--)
            {
                /* ce ni urejeno ter ce si elementa nista enaka, menjamo */
                /* primerjava vrednosti mora biti prva ker drugace se pri
                   enakih elementih primerjava nebi stela*/
                /* POPRAVEK ZARADI LALGEC-A:dejansko zadeva zaradi funkcijskega
                   klica traja predolgo in ga lalgec ubije predno sprocesira,
                   tako da moramo to odstraniti ter primerjave steti na roke */
                
                if(red ? tabela[i-1]>tabela[i] : tabela[i-1]<tabela[i])
                {
                    /* zamenjava zahteva 3 prirejanja */
                    int temp = tabela[i];
                    tabela[i] = tabela[i-1];
                    tabela[i-1] = temp;
                    stPrirejanj+=3;
                }
                stPrimerjav++;
            }
        }
        return tabela;
    }
    static int[] selectionSort(int[] tabela, boolean sled, boolean red)
    {
        /*  zahtevan selectionSort razdeli tabelo na urejen ter neurejen del,
            pri cemer v neurejenem delu poiscemo najvecji element ter ga dodamo
            na konec urejenega */
        for(int dolzUrejeni=0; dolzUrejeni<tabela.length; dolzUrejeni++)
        {
            if(sled)
            {
                /* ko gremo skozi celotno tabelo izpisemo novo stanje */
                for(int i=0; i<tabela.length; i++)
                    System.out.printf((i==dolzUrejeni ? "| %d ":"%d "), tabela[i]);
                System.out.println();
            }
            /* poiscemo po redu dolocen ekstrem v neurejenem delu */
            int ekstremIndeks=dolzUrejeni;
            
            /* elementa nam ni potrebno primerjati s samim sabo */
            for(int i=dolzUrejeni+1; i<tabela.length; i++)
            {
                /* poiscemo nasprotnega trenutnega vrstnemu vrstnemu redu
                   (ce urejamo narascajoce potem iscemo minimum) */
                if(urejen(tabela[i], tabela[ekstremIndeks], red))
                    ekstremIndeks = i;
            }
            /* v primeru, ko nam ostane le se en element, menjava
               sploh ni potrebna */
            if(dolzUrejeni < tabela.length-1)
            {
                /* zamenjamo ekstrem ter prvi neurejeni del tabele */
                int temp = priredi(tabela[dolzUrejeni]);
                tabela[dolzUrejeni] = priredi(tabela[ekstremIndeks]);
                tabela[ekstremIndeks] = priredi(temp);
            }
        }
        return tabela;
    }
    static int[] insertionSort(int[] tabela, boolean sled, boolean red)
    {
        /*  zahtevan insertionSort ima v zacetnem urejenem delu (z leve) ze
            vsebovan prvi element tabele. Urejeni del sirimo tako, da v
            urejeni del potisnemo prvi neurejeni element ter ga potiskamo
            v desno toliko casa, dokler ni ponovno vzpostavljen red */
        for(int dolzUrejeni=1; dolzUrejeni<=tabela.length; dolzUrejeni++)
        {
            if(sled)
            {
                /* ko gremo skozi celotno tabelo izpisemo novo stanje */
                for(int i=0; i<tabela.length; i++)
                    System.out.printf((i==dolzUrejeni ? "| %d ":"%d "), tabela[i]);
                if(dolzUrejeni==tabela.length)
                    System.out.print("| ");
                System.out.println();
            }

            /*  program zahteva izpis do urejene tabele, zato moramo posebej
                obravnavati, ko smo v koncni tabeli */
            if(dolzUrejeni<tabela.length)
            {
                /* dan neurejeni element, ki je tik za urejenim premikamo v levo,
                   dokler ni urejen oz. dokler ni enak - gremo do prvega elementa */
                int neurejenIndeks = dolzUrejeni;
                while(neurejenIndeks>0 && !urejen(tabela[neurejenIndeks-1], tabela[neurejenIndeks], red) && 
                      tabela[neurejenIndeks-1]!=tabela[neurejenIndeks])
                {
                    /* ce je neurejen glede na predhodnika, menjamo */
                    int temp = priredi(tabela[neurejenIndeks]);
                    tabela[neurejenIndeks] = priredi(tabela[neurejenIndeks-1]);
                    tabela[neurejenIndeks-1] = priredi(temp);
                    neurejenIndeks--;
                }
            }
        }
        return tabela;
    }
    /*  prirejena oblika heapSorta, ki ga je bilo potrebno oddati kot
        3. izziv */
    static int[] heapSort(int[] tabela, boolean sled, boolean red) {
        // gradnja kopice - gremo od zadnjega indeksa tabele proti prvemu
        // (s tem naredimo level order obhod od nizjega nivoja proti prvemu)
        // ter poskusamo pogrezniti trenutni element (oceta)
        for(int tEl=tabela.length-1; tEl>=0; tEl--)
            pogrezni(tabela, tEl, tabela.length, red);
        if(sled)
            izpisHeapsort(tabela, tabela.length);
        
        // dejansko urejanje - ko dobimo kopico, smo dobili na njenem vrhu
        // najvecji element, s premikom zacetnega ter (trenutnega) koncnega
        // elementa izbrisemo (po vrednosti trenutno najvecji) koren ter ga
        // nadomestimo z listom, nato pa ponovno pogrezamo
        for(int tMaxEl=0; tMaxEl<tabela.length-1; tMaxEl++)
        {
            // korene zamenjamo z listom
            int dolzKopice = tabela.length-tMaxEl-1;
            int tmp = priredi(tabela[0]);
            tabela[0] = priredi(tabela[dolzKopice]);
            tabela[dolzKopice] = priredi(tmp);
            // ponovno pogreznemo
            pogrezni(tabela, 0, dolzKopice, red);
            // izpisemo
            if(sled)
                izpisHeapsort(tabela, dolzKopice);
        }
        return tabela;
    }
    /*
    Metodo za pogrezanje lahko naredimo iterativno, glede na to da se moramo le
    sprehajati do otrok dokler ne dosezemo dna (tj. presezemo konec tabele)
    ali pa dokler ni pogoj zadoscen
    
    gradimo max kopico ce < (red=1), min kopico ce > (red=0)
    
    oce predstavlja zacetni indeks elementa, pri katerem zacnemo
    pogrezanje
    */
    static void pogrezni(int[] tabela, int indOce, int dolzKopice, boolean red) {
        int indSin; // indeks sina, s katerim menjamo
        while((indSin = indOce*2+1)<dolzKopice)
        {
            // nismo se prisli do konca, imamo vsaj se levega sina
            // zaenkrat imamo levega, preverimo se desnega
            if(indSin+1<dolzKopice && urejen(tabela[indSin],tabela[indSin+1],red))
               indSin++; // desni sin je vecji, uporabimo tega
            // pogledamo, ce je najvecji izmed sinov vecji od oceta,
            // v tem primeru menjamo ter poskusimo pogrezniti oceta se
            // naprej.
            // ce menjava ni potrebna, pomeni da je ta odsek ze urejen,
            // koncamo pogrezanje
            if(urejen(tabela[indOce], tabela[indSin], red))
            {
                int tmp = priredi(tabela[indSin]); // zamenjamo
                tabela[indSin] = priredi(tabela[indOce]);
                tabela[indOce] = priredi(tmp);
                // pogrezanje nadaljujemo v sinu
                indOce = indSin;
            }
            else
                return; // oce je glede na sinove urejen, koncamo pogrezanje
        }
    }
    static void izpisHeapsort(int[] tabela, int dolzKopice)
    {
        // koren je vedno na zacetku tabele
        for(int tEl=0, elNivoja=1, zacNivoja=tEl; tEl<dolzKopice; tEl++)
        {
            if(tEl-zacNivoja==elNivoja)
            {
                elNivoja *= 2;
                zacNivoja = tEl;
                System.out.print("| ");
            }
            System.out.print(tabela[tEl]+(tEl<dolzKopice-1?" ":"\n"));
        }
    }
    
    /* le klice dejansko funkcijo za quicksort, ki nato rekurzivno, obdela
       tabelo */
    static int[] quickSort(int[] tabela, boolean sled, boolean red)
    {
        /* ker se tabela prenasa po referenci, urejamo original, isto
           referenco vracamo zato da nekoliko skrajsamo skupno kodo za preverjanje
           stevila operacij */
        
        /* pozenemo z zacetnimi parametri (celotna tabela) */
        quickSortRek(tabela, 0, tabela.length-1, sled, red);
        return tabela;
    }
    /* rekurzivna funkcija */
    static void quickSortRek(int[] tabela, int leviIndeks, int desniIndeks, boolean sled, boolean red)
    {
        /* shranimo si prvotne pozicije indeksov */
        int prvotniLevi = leviIndeks;
        int prvotniDesni = desniIndeks;
        
        /* izvedemo prerazporeditev elementov glede na pivot */
        /*  za moje pojme edina smiselna priredba, ki bi se lahko zgodila v
            prvotnem algoritmu, je, da je potrebno prirediti pivot
            (glede na to, da sem kot prireditev ze v prejsnih algoritmih
             stel tudi prirejanje spremenljivki tmp, je to mozno)*/
        int pivot = priredi(tabela[(desniIndeks+leviIndeks)/2]); /* dolocimo pivotni (sredinski) element, ter si ga shranimo */
        while(leviIndeks <= desniIndeks)
        {
            while(urejen(tabela[leviIndeks],pivot, red)) leviIndeks++;
            while(urejen(pivot, tabela[desniIndeks], red)) desniIndeks--;
            
            if(leviIndeks <= desniIndeks)
            {
                //System.out.printf("menjava %d [%d] - %d [%d]\n",tabela[leviIndeks], leviIndeks, tabela[desniIndeks], desniIndeks);
                /*  v primeru neurejenosti zamenjamo prva elementa z (leve ter desne), ki
                    ne ustrezata pivotu */
                int tmp = priredi(tabela[leviIndeks]); // zamenjamo
                tabela[leviIndeks] = priredi(tabela[desniIndeks]);
                tabela[desniIndeks] = priredi(tmp);
                
                leviIndeks++;
                desniIndeks--;
            }
        }
        
        /* izpisemo rezultat delitve */
        if(sled)
        {
            /* lahko se zgodi, da nam je desni indeks manjsi od prvotnega
               levega */
            if(desniIndeks<prvotniLevi)
                System.out.print("| ");
            for(int i=prvotniLevi; i<=prvotniDesni; i++)
            {
                if(i==leviIndeks)
                    System.out.print("| ");
                System.out.print(tabela[i]+" ");
                if(i==desniIndeks)
                    System.out.print("| ");
            }
            System.out.println();
        }
        
        /*  kar je ostalo od levega ter desnega dela, rekurzivno urejamo,
            sredinski del ostane nespremenjen - prvo rekurzivno obdelamo
            levi, nato desni del
            to pocnemo seveda samo takrat, ko so indeksi se znotraj veljavnih mej
            tj. indeksa s katerima smo se sprehajali po tabeli, ne smeta preseci
            mej, ki sta jih postavila prvotna indeksa
           
            Prav tako, ker nima smisla, da urejamo obmocja z enim elementom,
            zahtevamo, da so meje novega obmocja strogo znotraj danega obmocja
            (s tem izlocimo koncne primere, kjer imamo bodisi le prvi ali pa
            le drugi element iz prvotnega obmocja dveh elementov)
        */
        if(prvotniLevi<desniIndeks)
            quickSortRek(tabela, prvotniLevi, desniIndeks, sled, red);
        if(prvotniDesni>leviIndeks)
            quickSortRek(tabela, leviIndeks, prvotniDesni, sled, red);
    }
    
    /*  naredimo notranji mergeSort - to implementiramo preko rekurzivnega algoritma,
        ki ga razredujemo od levih otrok proti desnim */
    static int[] mergeSort(int[] tabela, boolean sled, boolean red)
    {
        mergeSortRek(tabela, 0, tabela.length-1, sled, red);
        return tabela;
    }
    /*  enako kot pri quicksortu, levi indeks predstavlja prvi dostopen indeks,
        desni pa zadnjega */
    static void mergeSortRek(int[] tabela, int leviIndeks, int desniIndeks, boolean sled, boolean red)
    {
        /*  v primeru, da imamo le en element, je ta podinterval ze urejen in
            lahko vrnemo */
        if(leviIndeks==desniIndeks)
            return;
        /*  prvo razdelimo tabelo na 2 enaka dela - pri tem je dobljena meja
            prvi element desne tabele, oz. prvi nedosegljivi indeks leve */
        int meja = priredi((leviIndeks+desniIndeks)/2+1);
        /* izpisemo loceni tabeli */
        if(sled)
        {
            for(int i=leviIndeks; i<=desniIndeks; i++)
                System.out.printf((i==meja ? "| %d ":"%d "), tabela[i]);
            System.out.println();
        }
        
        /*  gremo rekurzivo razdeliti se na manjse polovice, ki se nato znotraj
            uredijo */
        mergeSortRek(tabela, leviIndeks, meja-1, sled, red);
        mergeSortRek(tabela, meja, desniIndeks, sled, red);
        /*  ko dobimo urejeni polovici, zlijemo njune elemente v novo tabelo,
            ter koncno (skupno) tabelo prepisemo nazaj v interval prvotne */
        int tmp[] = new int[desniIndeks-leviIndeks+1];
        int tmpIndeks = 0; /* indeks zacasne tabele */
        
        int leviPodIndeks = leviIndeks;  /* indeks podintervala */
        int desniPodIndeks = meja;
        /* zlivamo, dokler ne porabimo vseh elementov iz obeh podintervalov */
        while(leviPodIndeks<meja || desniPodIndeks<=desniIndeks)
        {
            /*  vzamemo element tistega podintervala, ki ustreza redu, ter indeks
                tega povecamo */
            /*  ce se levega podintervala nismo porabili ter ce njegov element
                ustreza redu, damo v skupno tabelo */
            if(leviPodIndeks<meja && (desniPodIndeks>desniIndeks || urejen(tabela[leviPodIndeks], tabela[desniPodIndeks], red)))
                tmp[tmpIndeks++]=priredi(tabela[leviPodIndeks++]);
            /*  drugace dodamo od desne - pogoja ni potrebno preverjati, ker se
                zunanja zanka prej zakljuci */
            else
                tmp[tmpIndeks++]=priredi(tabela[desniPodIndeks++]);
        }
        /* izpisemo zdruzeno tabelo */
        if(sled)
        {
            for(int i=0; i<tmp.length; i++)
                System.out.print(tmp[i]+" ");
            System.out.println();
        }
        /* zapisemo nazaj v prvotno tabelo */
        for(int i=0; i<tmp.length; i++)
            tabela[i+leviIndeks] = tmp[i];
    }
}

/* pomozni razred za nalogo, ki jo napolnemo pri vhodu, nato pa jo
   po koncu branja prepisemo v tabelo - ne ravno ucinkovito, vendar pa
   bolje kot pa podvajanje ter prepisovanje tabele*/
class Buffer
{
    int stEl = 0;

    /* dostopno le iz bufferja, buffer kot izhod vrne tabelo */
    class Element
    {
        Element naslednji;
        int vrednost;
        Element(int vrednost)
        {
            this.vrednost = vrednost;
        }
    }
    Element prviEl = null;
    Element konec = null;

    void dodajElement(int vrednost)
    {
        if(prviEl==null)
        {
            prviEl = new Element(vrednost);
            konec = prviEl;
        }
        /* vsaj prvi element ze imamo */
        else
        {
            Element nEl = new Element(vrednost);
            konec.naslednji = nEl;
            konec = nEl;
        }
        stEl++;
    }

    /* pretvori elemente bufferja v tabelo */
    int[] pretvori()
    {
        /* ustvarimo izhodno tabelo */
        int[] tabela = new int[stEl];
        Element tBuffer = prviEl;
        for(int tEl=0; tEl<stEl; tEl++)
        {
            tabela[tEl]=tBuffer.vrednost;
            tBuffer = tBuffer.naslednji;
        }
        return tabela;
    }
}